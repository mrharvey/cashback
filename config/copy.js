module.exports.tasks = {
  copy: {
    bowerlibs: {
      files: [{
        expand: true,
        cwd: "bower_components/jquery/dist",
        src: "jquery.min.js",
        dest: "<%= path.src %>/js/vendor/"
      }, {
        expand: true,
        cwd: "bower_components/modernizr/",
        src: "modernizr.js",
        dest: "<%= path.src %>/js/vendor/"
      }, {
        expand: true,
        cwd: "bower_components/requirejs/",
        src: "require.js",
        dest: "<%= path.src %>/js/vendor/"
      }]
    },

    bowercss: {
      files: [{
        expand: true,
        cwd: "bower_components/",
        src: "inuit-*/**/*.scss",
        dest: "<%= path.src %>/sass/inuit/",
        flatten: true
      }, {
        expand: true,
        cwd: "bower_components/bourbon",
        src: "**/*.scss",
        dest: "<%= path.src %>/sass/bourbon/",
        flatten: false
      }, {
        expand: true,
        cwd: "bower_components/neat",
        src: "**/*.scss",
        dest: "<%= path.src %>/sass/neat/",
        flatten: false
      }, {
        expand: true,
        cwd: "bower_components/c3",
        src: "c3.css",
        dest: "<%= path.src %>/sass/",
        flatten: false,
        rename: function(dest, src) {
          return dest + "_c3.scss";
        }
      }]
    },

    setupjs: {
      files: [{
        expand: true,
        cwd: "<%= path.src %>/js/vendor",
        src: ["modernizr-custom.js", "require.js"],
        dest: "<%= path.tmp %>/js/vendor"
      }]
    },


    fonts: {
      files: [{
        expand: true,
        cwd: "<%= path.src %>/fonts",
        src: ["*.{eot,svg,ttf,woff,woff2}"],
        dest: "<%= path.tmp %>/fonts"
      }]
    },

    svg: {
      files: [{
        expand: true,
        cwd: "<%= path.src %>/img/svg",
        src: ["*.svg"],
        dest: "<%= path.tmp %>/img/svg"
      },{
        expand: true,
        cwd: "<%= path.src %>/img/logos",
        src: ["*.svg"],
        dest: "<%= path.tmp %>/img/logos"
      }]
    },

    favicons: {
      files: [{
        expand: true,
        cwd: "<%= path.src %>/favicons/",
        src: ["*.*"],
        dest: "<%= path.tmp %>/"
      }]
    },

    prebuild: {
      files: [{
        expand: true,
        cwd: "<%= path.src %>/js/vendor",
        src: ["modernizr-custom.js", "require.js"],
        dest: "<%= path.tmp %>/js/vendor"
      }]
    },

    build: {
      files: [{
        expand: true,
        cwd: "<%= path.tmp %>/",
        src: "**/*",
        dest: "<%= path.dst %>/"
      }]
    }
  }
};
