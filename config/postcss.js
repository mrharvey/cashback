module.exports.tasks =  {
   postcss: {

      dev: {
         options: {
           map: true,
           processors: [
              require('autoprefixer-core')({browsers: ['> 5%', 'IE 9', 'IE 10', 'last 2 versions']}),
              require('postcss-discard-comments')({comments: {removeAll: true}})
           ]
         },
         src: '<%= path.tmp %>/css/prepro/screen.css',
         dest: '<%= path.tmp %>/css/screen.css'
      },

      build: {
         options: {
           map: false,
           processors: [
              require('autoprefixer-core')({browsers: ['> 5%', 'IE 9', 'IE 10', 'last 2 versions']}),
              require('postcss-discard-comments')({comments: {removeAll: true}})
           ]
         },
         src: '<%= path.tmp %>/css/prepro/screen.css',
         dest: '<%= path.tmp %>/css/screen.css'
      },

   }
};

