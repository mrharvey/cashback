module.exports.tasks = {

    sftp: {
        side: {
            files: {
                "./dist": "<%= path.dst %>/**"
            },
            options: {
                path: "~/www/cashback",
                host: "<%= secret.test.host %>",
                username: "<%= secret.test.username %>",
                password: "<%= secret.test.password %>",
                srcBasePath: "<%= path.dst %>/",
                // createDirectories: true,
                // directoryPermissions: 509
            }
        }
    }
};
