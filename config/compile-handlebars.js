module.exports.tasks = {

    'compile-handlebars': {

        compile: {
            template: '<%= path.src %>/htdocs/*.html',
            templateData: '<%= path.src %>/htdata/*.json',
            output: '<%= path.tmp %>/*.html',

            globals:  ['<%= path.src %>/htdata/globals/globals.json']
        }
    }
};
