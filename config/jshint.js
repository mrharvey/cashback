module.exports.tasks = {

    jshint: {

        options: {
            reporter: require('jshint-stylish'),
        },

        gruntfiles: {
            files: {
                src: [
                    'Gruntfile.js',
                    'config/*.{js,json}',
                    'tasks/*.{js,json}'
                ]
            },
        },

        js: {
            files: {
                src: [
                    'src/js/app.js',
                    'src/js/app/**/*.js'
                ]
            },

            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                browser: true,

                globals: {
                    // AMD
                    module: true,
                    require: true,
                    requirejs: true,
                    define: true,
                    escape: true,
                    // Environments
                    console: true,
                    // General Purpose Libraries
                    $: true,
                    jQuery: true,
                }
            }
        }
    }
};
