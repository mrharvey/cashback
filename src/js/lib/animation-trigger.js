define([
    "jquery",
    "odometer"
], function($, Odometer) {
    "use strict";

    return function() {

        this.cssActiveClass = "js-inview";
        this.positionThreshold = 0.4;

        var anims = [
        {element: $("#odometer"), func:alertOne}
        ];

        var self = this;



        this.check = function() {
            var windowHeight = $(window).height();
            var triggerPoint = (windowHeight * this.positionThreshold);
            var scrolled = $(window).scrollTop();

            for (var i = 0; i < anims.length; i++) {

                var $el = anims[i].element;
                if (!$el.hasClass(this.cssActiveClass)) {
                    var offset = $el.offset();
                    if (offset) {

                        var top = $el.offset().top;
                        var check = top - triggerPoint;

                        if (scrolled >= check) {
                            $el.addClass(this.cssActiveClass);

                            var associatedFunction = anims[i].func;
                            if (typeof associatedFunction === "function") {
                                associatedFunction();
                            }
                        }
                    }

                }
            }
        };

        //Set up the scroll listener -
        this.init = function() {
            //Handle window scrolling -
            if (document.addEventListener) {
                document.addEventListener("touchmove", onScrollHandler, false);
            }
            $(window).scroll(onScrollHandler);
            //Do an initial check -
            onScrollHandler();
        };

        function onScrollHandler() {
            self.check();
        }




        function alertOne() {


            od.update(number);
        }




    };
});
