/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
// (function() {

define(['lib/classie'], function(classie) {

    var internal;
    var container = document.getElementById('cd-main-content');

    if (document.referrer) {
        referrerUrl = document.referrer;
        referrer = referrerUrl.match(/:\/\/(.[^/|:]+)/)[1];
        internal = referrer === document.domain;
    }

    if (container && internal) {
        container.querySelector('header.ip-header').remove();
    }


    function noscroll() {
        window.scrollTo(0, 0);
    }


    function PathLoader(el) {
        this.el = el;
        this.el.style.strokeDasharray = this.el.style.strokeDashoffset = this.el.getTotalLength();
    }
    PathLoader.prototype._draw = function(val) {
        this.el.style.strokeDashoffset = this.el.getTotalLength() * (1 - val);
    };
    PathLoader.prototype.setProgress = function(val, callback) {
        this._draw(val);
        if (callback && typeof callback === 'function') {
            setTimeout(callback, 200);
        }
    };
    PathLoader.prototype.setProgressFn = function(fn) {
        if (typeof fn === 'function') {
            fn(this);
        }
    };
    window.PathLoader = PathLoader;


    var el = document.getElementById('cd-main-content');

    if (el && !(internal)) {

        var support = {
                animations: Modernizr.cssanimations
            },
            header = container.querySelector('header.ip-header'),
            loader = new PathLoader(document.getElementById('ip-loader-circle')),
            animEndEventNames = {
                'WebkitAnimation': 'webkitTransitionEnd',
                'OAnimation': 'oAnimationEnd',
                'msAnimation': 'MSAnimationEnd',
                'animation': 'animationend'
            },
            animEndEventName = animEndEventNames[Modernizr.prefixed('animation')];

        function init() {
            var onEndInitialAnimation = function() {
                if (support.animations) {
                    this.removeEventListener(animEndEventName, onEndInitialAnimation);
                }
                startLoading();
            };

            document.addEventListener('scroll', noscroll);

            classie.add(container, 'loading');

            if (support.animations) {
                container.addEventListener(animEndEventName, onEndInitialAnimation);
            } else {
                onEndInitialAnimation();
            }
        }


        function startLoading() {
            // simulate loading something..
            var simulationFn = function(instance) {

                var progress = 0,
                    interval = setInterval(function() {
                        progress = Math.min(progress + Math.random() * 0.9, 1);

                        instance.setProgress(progress);

                        // reached the end
                        if (progress === 1) {

                            classie.remove(container, 'loading');
                            classie.add(container, 'loaded');
                            clearInterval(interval);

                            if (window.matchMedia('(min-width: 720px)').matches) {
                                // slider
                                var swiper = new Swiper('#swiper-container-hero', {
                                    direction: 'horizontal',
                                    autoplay: 4000,
                                    slidesPerView: 1,
                                    spaceBetween: 0,
                                    nextButton: '.swiper-button-next',
                                    prevButton: '.swiper-button-prev'
                                });
                            }

                            var onEndHeaderAnimation = function(ev) {

                                document.removeEventListener('scroll', noscroll);

                                if (support.animations) {
                                    if (ev.target !== header) {
                                        return;
                                    }
                                    this.removeEventListener(animEndEventName, onEndHeaderAnimation);

                                }

                                classie.add(document.body, 'layout-switch');

                            };
                        }

                        if (support.animations) {
                            header.addEventListener(animEndEventName, onEndHeaderAnimation);

                        } else {
                            onEndHeaderAnimation();
                        }

                    }, 80);
            };

            loader.setProgressFn(simulationFn);
        }

        init();

    } else {

        if (window.matchMedia('(min-width: 720px)').matches) {
            // slider
            var swiper = new Swiper('#swiper-container-hero', {
                direction: 'horizontal',
                autoplay: 4000,
                slidesPerView: 1,
                spaceBetween: 0,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            });
        }
    }

});
