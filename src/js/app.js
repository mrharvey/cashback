// Place third party dependencies in the vendor folder
//
// Configure loading modules from the vendor directory,
// except 'app' ones,
requirejs.config({

    "baseUrl": "/js/",

    "paths": {
        "app": "app",
        "jquery": [
            "//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min",
            "vendor/jquery.min" // fallback to local version if CDN fails
        ],
        "swiper": "lib/jquery.swiper",
        "odometer": "lib/odometer",
        "animationtrigger": "lib/animation-trigger",
    },

    // Changing this to false strips out our comment too (set in 'wrap' in Gruntfile.js)
    preserveLicenseComments: true,
    optimize: 'uglify2'

});

// Load the main app module to start the app
requirejs(["app/main"]);
