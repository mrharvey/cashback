define([
  "jquery",
  'swiper',
  'animationtrigger',
  'odometer',
  'lib/unveil',
  'lib/jquery.nstSlider' //,
  // 'lib/loader'
], function(
  $, Swiper, AnimationTrigger, Odometer, unveil
) {

  $(function() {
    'use strict';

    var main = {

      init: function() {

        // IE detect
        function getInternetExplorerVersion() {
          var rv = -1;
          var ua = navigator.userAgent;
          var re;
          if (navigator.appName === 'Microsoft Internet Explorer') {
            re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null) {
              rv = parseFloat(RegExp.$1);
            }
          } else if (navigator.appName === 'Netscape') {
            re = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null) {
              rv = parseFloat(RegExp.$1);
            }
          }
          return rv;
        }
        var ieVer = getInternetExplorerVersion();

        if (ieVer > 1) {
          $('body').addClass('iedetected');
          $('body').addClass('iedetect-' + ieVer);
        } else {
          $('body').addClass('not-ie');
        }


        function toggleNav() {
          var navIsVisible = (!$('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
          $('.cd-dropdown').toggleClass('dropdown-is-active', navIsVisible);
          $('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
          if (!navIsVisible) {
            $('.cd-dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
              $('.has-children ul').addClass('is-hidden');
              $('.move-out').removeClass('move-out');
              $('.is-active').removeClass('is-active');
            });
          }
        }

        if (window.matchMedia("(min-width: 720px)").matches) {
          //open/close mega-navigation
          $('.cd-dropdown-trigger').on('click', function(event) {
            event.preventDefault();
            toggleNav();
          });
          //close meganavigation
          $('.cd-dropdown .cd-close').on('click', function(event) {
            event.preventDefault();
            toggleNav();
          });
        }




        //  odometer
        var el = document.querySelector('#odometer');
        if (el) {
          // var animationTriggerer = new AnimationTrigger();
          //Triggered functions -
          var ele = document.querySelector("#odometer");
          var number = $(ele).data("number");
          var od = new Odometer({
            el: ele,
            value: "00000",
            format: "(,ddd)",
            theme: "minimal"
          });
          od.update(number);
        }



        function tabTitle(scope) {
          scope.siblings(".active").removeClass("active");
          scope.addClass("active");

          var index = scope.index();
          var tabs = $(".tabs-content .content");
          var order = $(".product--order__item");
          var info = $(".product--info");

          tabs.removeClass("active");
          order.removeClass("active");
          info.removeClass(function(index, css) {
            return (css.match(/(^|\s)infoactive-\S+/g) || []).join(' ');
          });

          tabs.eq(index).addClass("active");
          order.eq(index).addClass("active");
          info.addClass("infoactive-" + index);
        }

        // tabs
        $(".tab-title:not(.contact)").on("click", function(e) {
          e.preventDefault();
          tabTitle($(this));
        });
        $(".tab-title:not(.contact)").on("mouseover", function(e) {
          tabTitle($(this));
        });



        // iframes
        var iframeString = '<iframe frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen />';

        function loadDeferredIframe() {
          $('.iframe-placeholder').each(function() {

            var that = $(this);
            var iframeSrc = $(this).data('src');
            var timeoutID;

            var loadIframe = $(iframeString).attr('src', iframeSrc);
            loadIframe.appendTo(this);

            function delayedSwitch() {
              timeoutID = window.setTimeout(function() {
                that.removeClass('preload');
              }, 1000);
            }
            delayedSwitch();

          });
        }
        loadDeferredIframe();



        // calulator radial

        function SVG(tag) {
          return document.createElementNS('http://www.w3.org/2000/svg', tag);
        }

        function drawSVGPaths(_parentElement, amount, _timeMin, _timeMax, _timeDelay) {
          var paths = $(_parentElement);

          //for each PATH..
          $.each(paths, function(i) {
            //get the total length
            var totalLength = this.getTotalLength();
            var amountPct = (1 - (amount));

            //set PATHs to invisible
            $(this).css({
              'stroke-dashoffset': totalLength,
              'stroke-dasharray': totalLength + ' ' + totalLength
            });

            //animate
            $(this).css({
              'stroke-dashoffset': totalLength * amountPct
            });

          });
        }

        function startSVGAnimation(parentElement, amount) {
          drawSVGPaths(parentElement, amount, 1800, 2000, 50);
        }

        // calculator
        var calc1 = 0;
        var calc2 = 0;
        var calc3 = 0;

        function calculatorTotals() {
          var total = calc1 + calc2 + calc3;
          if (total === 750) {
            total = ((total * 12) * 0.11).toFixed(2);
            $('#calculator--total span').text(total);
          } else {
            total = ((total * 12) * 0.10).toFixed(2);
            $('#calculator--total span').text(total);
          }
        }

        // slider
        $('#nstSlider1').nstSlider({
          "left_grip_selector": ".leftGrip",
          "value_bar_selector": ".bar",
          "rounding": 5,

          "value_changed_callback": function(cause, leftValue) {
            var $container = $(this).parent(),
              g = 0 + leftValue,
              r = 255 - g,
              b = 0;
            calc1 = leftValue;
            $(this).parent('.nstHolder').next('.leftLabel').text(leftValue);
            $(this).find('.bar').css('background', 'rgb(' + [r, g, b].join(',') + ')');
            $('#calctotal--food').text(leftValue * 12);
            $('#radial-read-1').text(leftValue);

            calculatorTotals();
            startSVGAnimation($('#PATH1'), (calc1 / 250));
          }
        });
        // slider
        $('#nstSlider2').nstSlider({
          "left_grip_selector": ".leftGrip",
          "value_bar_selector": ".bar",
          "rounding": 5,
          "value_changed_callback": function(cause, leftValue) {
            var $container = $(this).parent(),
              g = 0 + leftValue,
              r = 255 - g,
              b = 0;
            calc2 = leftValue;
            $(this).parent('.nstHolder').next('.leftLabel').text(leftValue);
            $(this).find('.bar').css('background', 'rgb(' + [r, g, b].join(',') + ')');
            $('#calctotal--travel').text(leftValue * 12);
            $('#radial-read-2').text(leftValue);

            calculatorTotals();
            startSVGAnimation($('#PATH2'), (calc2 / 250));

          }
        });
        // slider
        $('#nstSlider3').nstSlider({
          "left_grip_selector": ".leftGrip",
          "value_bar_selector": ".bar",
          "rounding": 5,
          "value_changed_callback": function(cause, leftValue) {
            var $container = $(this).parent(),
              g = 0 + leftValue,
              r = 255 - g,
              b = 0;
            calc3 = leftValue;
            $(this).parent('.nstHolder').next('.leftLabel').text(leftValue);
            $(this).find('.bar').css('background', 'rgb(' + [r, g, b].join(',') + ')');
            $('#calctotal--other').text(leftValue * 12);
            $('#radial-read-3').text(leftValue);
            calculatorTotals();
            startSVGAnimation($('#PATH3'), (calc3 / 250));

          }
        });

        // hero slider
        if (window.matchMedia('(min-width: 720px)').matches) {
            var swiper = new Swiper('#swiper-container-hero', {
                direction: 'horizontal',
                autoplay: 4000,
                slidesPerView: 1,
                spaceBetween: 0,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            });
        }

        // off canvas
        if (window.matchMedia("(max-width: 719px)").matches) {

          var $lateral_menu_trigger = $('#cd-menu-trigger'),
            $content_wrapper = $('.cd-main-content'),
            $navigation = $('header');

          //open-close lateral menu clicking on the menu icon
          $lateral_menu_trigger.on('click', function(event) {
            event.preventDefault();

            $lateral_menu_trigger.toggleClass('is-clicked');
            $navigation.toggleClass('lateral-menu-is-open');
            $content_wrapper.toggleClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
              // firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
              $('body').toggleClass('overflow-hidden');
            });
            $('#cd-lateral-nav').toggleClass('lateral-menu-is-open');

            //check if transitions are not supported - i.e. in IE9
            if ($('html').hasClass('no-csstransitions')) {
              $('body').toggleClass('overflow-hidden');
            }
          });

          //close lateral menu clicking outside the menu itself
          $content_wrapper.on('click', function(event) {
            if (!$(event.target).is('#cd-menu-trigger, #cd-menu-trigger span')) {
              $lateral_menu_trigger.removeClass('is-clicked');
              $navigation.removeClass('lateral-menu-is-open');
              $content_wrapper.removeClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                $('body').removeClass('overflow-hidden');
              });
              $('#cd-lateral-nav').removeClass('lateral-menu-is-open');
              //check if transitions are not supported
              if ($('html').hasClass('no-csstransitions')) {
                $('body').removeClass('overflow-hidden');
              }

            }
          });

          //open (or close) submenu items in the lateral menu. Close all the other open submenu items.
          $('.item-has-children').children('a').on('click', function(event) {
            event.preventDefault();
            $(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(200);
          });

        }



        $('img.lazy').unveil(200);


      }
    };
    main.init();

  });

});
