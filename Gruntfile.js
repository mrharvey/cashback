module.exports = function(grunt) {

    require('time-grunt')(grunt);

    // Dynamically load tasks
    // JiT Grunt replaces MatchDep - https://www.npmjs.com/package/jit-grunt
    require('jit-grunt')(grunt, {
        express: 'grunt-express-server',
        sftp: 'grunt-ssh'
    });

    // Load from configs/ dir
    var configs = require('load-grunt-configs')(grunt);

    // Get contents of package.json
    configs.pkg = grunt.file.readJSON('package.json');

    // Load passwords etc.
    var findup = require('findup-sync');
    var secretPath = findup('homesec.json', {
        nocase: true
    });
    if (secretPath === null) {
        grunt.fatal("Can't find secret.json");
    }
    configs.secret = grunt.file.readJSON(secretPath);

    grunt.initConfig(configs);

    // Custom tasks
    grunt.loadTasks('tasks');



    grunt.registerTask('default', []);

    grunt.registerTask('bower', 'Pull bower packages and copy them across', ['bowerInstall', 'copy:bowerlibs', 'copy:bowercss', 'copy:setupjs']);
    grunt.registerTask('setup', 'Setting up external sites and repositories', ['clean:setup', 'bower']);

    grunt.registerTask('dev', 'Active development phase', ['dosvg', 'copy:fonts', 'sass', 'copy:favicons', 'processhtml:dev', "requirejs", 'express', 'browserSync', 'watch']);
    grunt.registerTask('build', 'General build task', ['clean:predist', 'copy:fonts', 'sass', 'postcss:build', 'compile-handlebars', 'processhtml:dist', 'imagemin', 'dosvg', 'copy:favicons', 'copy:prebuild', 'requirejs', 'copy:build']);

    grunt.registerTask('live', 'Building for live distribution', ['bump:major', 'build', 'processhtml:dist']);

    grunt.registerTask('dosvg', 'Optimise and convert SVGs', ['copy:svg']); //'svg2png',


};
